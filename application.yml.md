## mariadb를 사용할 경우
```yaml
spring:
  datasource:
    driverClassName: org.mariadb.jdbc.Driver
    url: jdbc:mariadb://localhost:3306/account
    username: account
    password: account
  jpa:
    hibernate:
      ddl-auto: update
    properties:
      hibernate:
        show_sql: true
        format_sql: true
```

## h2를 사용할 경우
```yaml
spring:
  application:
    name: account
  h2:
    console:
      enabled: true
      path: /h2
  jpa:
    properties:
      hibernate:
        show_sql: true
        format_sql: true
  datasource:
    url: jdbc:h2:mem:account
    driverClassName: org.h2.Driver
    username: sa
    password:
```
